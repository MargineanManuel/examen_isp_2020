package ex1;

import java.util.List;

class A {
}

class B extends A {
    private String param;

    public B(String param) {
        this.param = param;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
}

class Z {
    public String g() {
        B b = new B("ciocolata");
        return b.getParam();
    }
}

class C {
    List<B> Bs;
    public C(B bComp) {
        this.Bs.add(bComp);
    }

    @Override
    public String toString() {
        return "C has " + "B's : " + Bs.toString();
    }
}

class E {
    private B b;
    public E(B b) {
        this.b = b;
    }
    @Override
    public String toString() {
        return "E{" + "b=" + b + '}';
    }
}

class D {
    public void f(){
        B b1 = new B("One");
        B b2 = new B("To");
        B b3 = new B("Many");
    }
}


