package ex2;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ex2 {

    Color buttColor = new Color(97,103,113);
    Font buttFont = new Font("Arial",Font.PLAIN, 20);
    Color bgColor = new Color(223, 227, 238);
    Color buttHover = new Color(66, 103, 179);
    public JFrame frame;

    public ex2() {
        frame = new JFrame();
        frame.setTitle("Marginean Manuel - Ex2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 500);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(bgColor);

        JButton button1 = new JButton("Copy text");
        button1.setContentAreaFilled(false);
        button1.setBounds(250,320,100,50);
        button1.setFont(buttFont);
        button1.setBorder(new LineBorder(buttColor,4));
        button1.setForeground(buttColor);
        panel.add(button1);

        JTextArea textArea = new JTextArea();
        textArea.setBounds(150,170,300,120);
        textArea.setEditable(false);

        JTextField textField = new JTextField();
        textField.setBounds(150,30,300,120);

        panel.add(textField);
        panel.add(textArea);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.setText((textField.getText()));
            }
        });

        button1.addMouseListener(new MouseAdapter() {
            Color color = button1.getForeground();
            public void mouseEntered(MouseEvent me) {
                color = button1.getForeground();
                button1.setForeground(buttHover);
                button1.setBorder(new LineBorder(buttHover,4));
            }
            public void mouseExited(MouseEvent me) {
                button1.setForeground(color);
                button1.setBorder(new LineBorder(buttColor,4));
            }
        });

        frame.add(panel);
        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        ex2 ex2 = new ex2();
    }
}